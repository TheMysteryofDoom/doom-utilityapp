import discord
import re
from discord.ext import commands
try: # check if BeautifulSoup4 is installed
    from bs4 import BeautifulSoup
    soupAvailable = True
except:
    soupAvailable = False
import aiohttp

class ServantSkills:
    """This is Homura's CirnoRipper Features!"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def testfield(self, *, searchArg):
        """Grabs a servant picture"""
        #Your code will go here
        url = "http://fategrandorder.wikia.com/wiki/"+searchArg
        async with aiohttp.get(url) as response:
            soup = BeautifulSoup(await response.text(), 'html.parser')
        try:
            #target = soup.find("div", {"class": "ServantInfoStatsWrapper"})
            #image = soup.find("a", title="Stage 4")
            #target = soup.find("div", title="First Skill")
            stat = soup.find(string=re.compile('Available from the start')).parent.parent.parent.find_next('td').find_next('td')
            #stat = soup.find('a',title="Traits").parent.parent.get_text()
            
            #await self.bot.say(image['href'])
            await self.bot.say(stat)
        except Exception as ex:
            await self.bot.say(ex)

def setup(bot):
    if soupAvailable:
        bot.add_cog(ServantSkills(bot))
    else:
        raise RuntimeError("You need to run `pip3 install beautifulsoup4`")

