import discord
import re
from discord.ext import commands
try: # check if BeautifulSoup4 is installed
    from bs4 import BeautifulSoup
    soupAvailable = True
except:
    soupAvailable = False
import aiohttp

class ServantStats:
    """This is Homura's CirnoRipper Features!"""

    def __init__(self, bot):
        self.bot = bot
        self.searchHelper = {'billy':'Billy_The_Kid',
                             'ecchan':'Mysterious_Heroine_X_(Alter)',
                             'artoria':'Artoria_Pendragon',
                             'artoria alter':'Artoria_Pendragon_(Alter)',
                             'artoria_alter':'Artoria_Pendragon_(Alter)'}

    @commands.command()
    async def servant(self, *, searcharg):
        """servant stats displayed in blocks"""
        # Call a Search Refiner Here
        searcharg.replace(" ", "_")
        if searcharg.lower() in self.searchHelper:
            searcharg = self.searchHelper[searcharg.lower()]
        else:
            pass
        # ==========================
        url = "http://fategrandorder.wikia.com/wiki/"+searcharg
        async with aiohttp.get(url) as response:
            soup = BeautifulSoup(await response.text(), 'html.parser')
        try:
            base = soup.find("div", {"class": "ServantInfoStatsWrapper"})
            image = soup.find("a", title="Stage 4")
            tableset = base.find_all("table", {"class": "closetable"})
            # ================
            stat = soup.find("p", {"class": "ServantInfoName"}).get_text()
            embed = discord.Embed(title="Servant", description=stat, color=0xdb3c33)
            embed.set_image(url=image['href'])
            # ================
            tablework = tableset[1]
            td_only = tablework.find_all("td")
            # ================
            td_test = td_only[0].get_text() #ID
            embed.add_field(name="ID", value=td_test[5:], inline=True)
            # ================
            td_test = td_only[1].get_text() #Cost
            stat = td_test[7:] 
            if int(stat) == 7:
                stat = "★★★"
            elif int(stat) == 12:
                stat = "★★★★"
            elif int(stat) == 16:
                stat = "★★★★★"
            elif int(stat) == 3:
                stat = "★"
            elif int(stat) == 4:
                stat = "★★"
            embed.add_field(name="Rarity", value=stat, inline=True)
            # ================ Attack
            td_test = td_only[2].get_text() #Attack
            embed.add_field(name="Attack (Min/Max)", value=td_test[7:], inline=True)
            # ================ HP
            td_test = td_only[3].get_text() #HP
            embed.add_field(name="HP (Min/Max)", value=td_test[5:], inline=True)
            # ================ G ATK
            stat = soup.find(string=re.compile('Grail HP')).parent.parent.parent.find_previous('span').get_text() #Grail ATK
            embed.add_field(name="Grail ATK (Lv. 100)", value=stat, inline=True)
            # ================ G HP
            stat = soup.find(string=re.compile('Grail HP')).parent.parent.parent.find_next('span').find_next('span').get_text() #Grail HP
            embed.add_field(name="Grail HP (Lv. 100)", value=stat, inline=True)
            # ================ Voice Actor
            stat = soup.find(string=re.compile('Voice Actor')).parent.parent.parent.get_text() #VoiceActor
            embed.add_field(name="Voice Actor", value=stat[13:], inline=True)
            # ================ Illustrator
            stat = soup.find(string=re.compile('Voice Actor')).parent.parent.parent.find_next('td').get_text() #Illustrator
            embed.add_field(name="Illustrator", value=stat[14:], inline=True)
            # ================ Alignment
            stat = soup.find('a',title="Alignments").parent.parent.get_text() #Alignment
            embed.add_field(name="Alignment", value=stat[12:], inline=True)
            # ================ Gender
            stat = soup.find(string=re.compile('Gender')).parent.parent.get_text()
            embed.add_field(name="Gender", value=stat[8:], inline=True)
            # ================ Traits
            stat = soup.find('a',title="Traits").parent.parent.get_text()
            embed.add_field(name="Traits", value=stat[10:], inline=True)
            # ================
            stat = soup.find("p", {"class": "ServantInfoClass"})
            image = stat.find_next('img')
            embed.set_thumbnail(url=image['data-src'])
            # ================
            # embed.add_field(name="TestTitle", value="hi2", inline=True)
            await self.bot.say(embed=embed)
        except Exception as ex:
            await self.bot.say(ex)

def setup(bot):
    if soupAvailable:
        bot.add_cog(ServantStats(bot))
    else:
        raise RuntimeError("You need to run `pip3 install beautifulsoup4`")

