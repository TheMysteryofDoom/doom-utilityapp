import discord
from random import choice
from discord.ext import commands

class HomuraAdvisor:
    """This is Homura's Advisor Features!"""

    def __init__(self, bot):
        self.bot = bot
        self.homuwords = ["Have you summoned a new servant yet? Why don't you try your luck...", 
                       "Head to Chaldea Gate and take down a few hands", 
                       "Gather some Mana Prims. Who knows when you might need it...", 
                       "I suggest you head to the Treasure Vault.", 
                       "Every servant has it's use, even Fionn.", 
                       "If you haven't finished the story missions yet, you should do that right now.", 
                       "...",
                       "Relax for a moment",
                       "Don't believe in Kyubey's lies.",
                       "Save your quartz."]

    @commands.command()
    async def advice(self):
        """Homura gives advice"""

        #Your code will go here
        await self.bot.say(choice(self.homuwords))

    @commands.command()
    async def proof(self):
        """Debug Line"""

        #Your code will go here
        await self.bot.say("I'm definitely active")


def setup(bot):
    bot.add_cog(HomuraAdvisor(bot))

