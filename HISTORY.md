# Utility App

#### Version 0
    * Repository Created

#### Version 0.20
    * Basic Cog Created

#### Version 0.50
    * Implemented Main Cog, now under development

#### Version 1.00
    * Finished Main Cog Features

#### Version 1.00.5
    * Added Servant Icon to ServantStats Cog
